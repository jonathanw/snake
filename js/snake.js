/**
 * Original code by Nick Morgan (http://skilldrick.co.uk/)
 * CSS Tricks Article: https://css-tricks.com/learn-canvas-snake-game/
 * 
 * Alterations, Design: Jonathan Wagener (http://plaas.co)
 */

var JS_SNAKE = {};

var input_queue = [];

JS_SNAKE.equalCoordinates = function (coord1, coord2) {
  return coord1[0] === coord2[0] && coord1[1] === coord2[1];
}

JS_SNAKE.checkCoordinateInArray = function (coord, arr) {
  var isInArray = false;
  $.each(arr, function (index, item) {
    if (JS_SNAKE.equalCoordinates(coord, item)) {
      isInArray = true;
    }
  });
  return isInArray;
};

JS_SNAKE.game = (function () {
  var canvas, ctx;
  var frameLength;
  var snake;
  var apple;
  var badapple;
  var score;
  var hiscore;
  var timeout;

  console.log($(window).width());

  var _w_width = $(window).width() - 40;
  var _w_height = $(window).height() - 120;

  JS_SNAKE.width = _w_width;
  JS_SNAKE.height = _w_height;

  JS_SNAKE.blockSize = parseInt(_w_width * 0.032);

  console.log(JS_SNAKE.blockSize);

  // Make sure that the width is an even number.
  var _wwo = JS_SNAKE.width % JS_SNAKE.blockSize;
  var _who = JS_SNAKE.height % JS_SNAKE.blockSize;

  if (_wwo > 0) {
    JS_SNAKE.width = _w_width - _wwo;
  }

  if (_who > 0) {
    JS_SNAKE.height = _w_height - _who;
  }

  $("header, #middle, footer").css("width", JS_SNAKE.width);

  JS_SNAKE.widthInBlocks = JS_SNAKE.width / JS_SNAKE.blockSize;
  JS_SNAKE.heightInBlocks = JS_SNAKE.height / JS_SNAKE.blockSize;

  function init() {
    var $canvas = $('#snake');

    $canvas.attr('width', JS_SNAKE.width);
    $canvas.attr('height', JS_SNAKE.height);

    console.log($canvas);

    canvas = $canvas[0];
    ctx = canvas.getContext('2d');

    score = 0;
    hiscore = 0;

    if (localStorage.hiscore > 0) {
      hiscore = localStorage.hiscore;
    }

    frameLength = 200;

    snake = JS_SNAKE.snake();
    apple = JS_SNAKE.apple();
    badapple = JS_SNAKE.badapple();

    bindEvents();
    gameLoop();
  }

  function gameLoop() {
    ctx.clearRect(0, 0, JS_SNAKE.width, JS_SNAKE.height);
    snake.advance(apple);
    draw();

    if (snake.checkCollision(badapple)) {
      snake.retreat(); //move snake back to previous position
      snake.draw(ctx); //draw snake in its previous position
      gameOver();
    }
    else {
      timeout = setTimeout(gameLoop, frameLength);
    }
  }

  function draw() {
    snake.draw(ctx);
    //drawBorder();
    apple.draw(ctx);
    badapple.draw(ctx);
    drawScore();
  }

  function pad_number(num, size) {
    var s = num +"";
    while (s.length < size) s = "0" + s;
    return s;
  }

  function drawScore() {
    ctx.save();
    $("span.current_score").text(pad_number(score.toString(), 5));
    $("span.hi_score").text(pad_number(hiscore.toString(), 5));
    ctx.restore();
  }

  function gameOver() {
    ctx.save();
    $(".gameover").show();
    ctx.restore();
  }

  function restart() {
    clearTimeout(timeout);
    $(".gameover").hide();
    $('body').unbind('keydown');
    $(JS_SNAKE).unbind('appleEaten');
    $(canvas).unbind('click');
    JS_SNAKE.game.init();
  }
  
  function bindEvents() {
    var keysToDirections = {
      37: 'left',
      38: 'up',
      39: 'right',
      40: 'down'
    };

    // Implement a touch area.
    $(document).on('touchstart click', function(event){
      event.stopPropagation();
      event.preventDefault();

      if(event.handled !== true) {
        var _target = $(event.target);
        var direction;

        if (_target.attr("id") === "snake") {
          console.log($(event.target));
          console.log(event);

          if (event.offsetX != "undefined") {
            var _e_offset_x = event.offsetX;
            var _e_offset_y = event.offsetY;
          }
          else {
            var _e_offset_x = parseInt(event.touches[0].clientX - event.target.offsetLeft);
            var _e_offset_y = parseInt(event.touches[0].clientY - event.target.offsetTop);
          }

          console.log(_e_offset_x);
          console.log(_e_offset_y);

          var _c_width = $("canvas#snake").width();
          var _c_height = $("canvas#snake").height();

          if (_e_offset_y <= (_c_height * .25)) {
            direction = "up";
          }
          else if (_e_offset_y >= (_c_height * .75)) {
            direction = "down";
          }
          else if (_e_offset_y > (_c_height * .25) && _e_offset_y < (_c_height * .75) && _e_offset_x <= (_c_width * .25)) {
            direction = "left";
          }
          else if (_e_offset_y > (_c_height * .25) && _e_offset_y < (_c_height * .75) && _e_offset_x >= (_c_width * .75)) {
            direction = "right";
          }

          if (direction) {
            input_queue.push(direction);
            snake.setDirection(direction);

            event.handled = true;
            event.preventDefault();
          }
        }
      }
      else {
        return false;
      }
    });

    $(document).keydown(function (event) {
      var key = event.which;
      var direction = keysToDirections[key];

      if (direction) {
        input_queue.push(direction);
        snake.setDirection(direction);
        event.preventDefault();
      }
      else if (key === 32) {
        restart();
      }
    });

    $(JS_SNAKE).bind('appleEaten', function (event, snakePositions) {
      apple.setNewPosition(snakePositions);
      score++;

      if (score > hiscore) {
        hiscore = score;
        localStorage.hiscore = hiscore;
      }

      frameLength *= 0.99; //subtle speed-up
    });
  }

  return {
    init: init
  };
})();

JS_SNAKE.apple = function () {
  //get a random position within the game bounds
  function getRandomPosition() {
    var x = random(1, JS_SNAKE.widthInBlocks - 2);
    var y = random(1, JS_SNAKE.heightInBlocks - 2);
    return [x, y];
  }

  var position = getRandomPosition();

  function draw(ctx) {
    ctx.save();
    ctx.fillStyle = '#FF4E50'; //apple red
    ctx.beginPath();
    var radius = JS_SNAKE.blockSize / 2;
    var x = position[0] * JS_SNAKE.blockSize + radius;
    var y = position[1] * JS_SNAKE.blockSize + radius;
    ctx.arc(x, y, radius, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.restore();
  }

  function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
  }

  function setNewPosition(snakeArray) {
    var newPosition = getRandomPosition();
    //if new position is already covered by the snake, try again
    if (JS_SNAKE.checkCoordinateInArray(newPosition, snakeArray)) {
      return setNewPosition(snakeArray);
    }
    //otherwise set position to the new position
    else {
      position = newPosition;
    }
  }

  function getPosition() {
    return position;
  }

  return {
    draw: draw,
    setNewPosition: setNewPosition,
    getPosition: getPosition
  };
};

JS_SNAKE.badapple = function () {
  //get a random position within the game bounds
  function getRandomPosition() {
    var x = random(1, JS_SNAKE.widthInBlocks - 2);
    var y = random(1, JS_SNAKE.heightInBlocks - 2);
    return [x, y];
  }

  var position = getRandomPosition();

  function draw(ctx) {
    ctx.save();
    ctx.fillStyle = '#F9D423';
    ctx.beginPath();
    var radius = JS_SNAKE.blockSize / 2;
    var x = position[0] * JS_SNAKE.blockSize + radius;
    var y = position[1] * JS_SNAKE.blockSize + radius;
    ctx.arc(x, y, radius, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.restore();
  }

  function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
  }

  function setNewPosition() {
    var newPosition = getRandomPosition();
    position = newPosition;
  }

  function getPosition() {
    return position;
  }

  return {
    draw: draw,
    setNewPosition: setNewPosition,
    getPosition: getPosition
  };
};

JS_SNAKE.snake = function () {
  var previousPosArray;
  var posArray = [];

  posArray.push([6, 4]);
  posArray.push([5, 4]);

  var direction = 'right';
  var nextDirection = direction;

  function setDirection(newDirection) {
    var allowedDirections;

    switch (direction) {
      case 'left':
      case 'right':
        allowedDirections = ['up', 'down'];
        break;
      case 'up':
      case 'down':
        allowedDirections = ['left', 'right'];
        break;
      default:
        throw('Invalid direction');
    }

    if (allowedDirections.indexOf(newDirection) > -1) {
      nextDirection = newDirection;
    }
  }

  function drawSection(ctx, position) {
    var x = JS_SNAKE.blockSize * position[0];
    var y = JS_SNAKE.blockSize * position[1];
    ctx.fillRect(x, y, JS_SNAKE.blockSize, JS_SNAKE.blockSize);
  }

  function draw(ctx) {
    ctx.save();
    ctx.fillStyle = '#A7CD2C';

    for(var i = 0; i < posArray.length; i++) {
      drawSection(ctx, posArray[i]);
    }
    ctx.restore();
  }

  function checkCollision(badapple) {
    var wallCollision = false;
    var snakeCollision = false;
    var badAppleCollision = false;

    var head = posArray[0]; //just the head
    var rest = posArray.slice(1); //the rest of the snake

    var snakeX = head[0];
    var snakeY = head[1];

    var minX = 0;
    var minY = 0;

    var maxX = JS_SNAKE.widthInBlocks;
    var maxY = JS_SNAKE.heightInBlocks;

    var outsideHorizontalBounds = snakeX < minX || snakeX >= maxX;
    var outsideVerticalBounds = snakeY < minY || snakeY >= maxY;

    if (outsideHorizontalBounds || outsideVerticalBounds) {
      wallCollision = true;
    }

    //check if the snake head coords overlap the rest of the snake
    snakeCollision = JS_SNAKE.checkCoordinateInArray(head, rest);

    if (isEatingBadApple(posArray[0], badapple)) {
      badAppleCollision = true;
    }

    return wallCollision || snakeCollision || badAppleCollision;
  }

  function advance(apple) {
    //make a copy of the head of the snake otherwise
    //the changes below would affect the head of the snake
    var nextPosition = posArray[0].slice();
    direction = nextDirection;
    switch (direction) {
    case 'left':
      nextPosition[0] -= 1;
      break;
    case 'up':
      nextPosition[1] -= 1;
      break;
    case 'right':
      nextPosition[0] += 1;
      break;
    case 'down':
      nextPosition[1] += 1;
      break;
    default:
      throw('Invalid direction');
    }

    previousPosArray = posArray.slice(); //save previous array
    posArray.unshift(nextPosition);
    if (isEatingApple(posArray[0], apple)) {
      $(JS_SNAKE).trigger('appleEaten', [posArray]);
    }
    else {
      posArray.pop();
    }
  }

  function isEatingApple(head, apple) {
    return JS_SNAKE.equalCoordinates(head, apple.getPosition());
  }

  function isEatingBadApple(head, badapple) {
    return JS_SNAKE.equalCoordinates(head, badapple.getPosition());
  }

  function retreat() {
    posArray = previousPosArray;
  }

  return {
    draw: draw,
    advance: advance,
    retreat: retreat,
    setDirection: setDirection,
    checkCollision: checkCollision
  };
};


JS_SNAKE.game.init();