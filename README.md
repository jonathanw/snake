# snake

<p align="center">
	<img src="https://gitlab.com/jonathanw/snake/-/raw/master/img/screenshot.jpg" alt="Snake Screenshot" width="250">
</p>

<!--<p align="center">
	<a href="http://theamoeba.github.io/snake/index.htm">Play Online</a>
</p>-->

A silly little canvas game as my first real look 
into building online Javascript games.

# Gameplay

- Eat all the red apples
- Avoid all of the yellow apples. They are poisened and result in the game ending immediately.
- Don't touch the walls

## Credit

Original code: Nick Morgan (http://skilldrick.co.uk/)

CSS Tricks Article: https://css-tricks.com/learn-canvas-snake-game/

All alterations, design etc: Jonathan Wagener (http://plaas.co)